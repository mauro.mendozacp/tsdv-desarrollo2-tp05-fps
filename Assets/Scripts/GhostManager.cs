﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostManager : MonoBehaviour
{
    [SerializeField] private float spawnDelay;
    [SerializeField] private GameObject ghostPrefab;

    [SerializeField] private Vector3 minPosition;
    [SerializeField] private Vector3 maxPosition;
    [SerializeField] private float offsetX;
    [SerializeField] private float offsetY;

    private List<Ghost> ghostList = new List<Ghost>();
    private float timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        Ghost.OnGhostKilledAsStatic += GhostDied;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > spawnDelay)
        {
            timer = 0;

            GameObject ghost = Instantiate(ghostPrefab);

            Ghost g = ghost.GetComponent<Ghost>();

            ghost.name = "Ghost " + (ghostList.Count + 1);
            ghost.transform.position = GetRandomPosition();
            ghost.transform.parent = transform;

            ghostList.Add(g);
        }
    }

    public Vector3 GetRandomPosition()
    {
        Vector3 auxPos = Vector3.zero;
        bool repeat = false;

        do
        {
            repeat = false;

            auxPos.x = Random.Range(minPosition.x, maxPosition.x);
            auxPos.y = minPosition.y;
            auxPos.z = Random.Range(minPosition.z, maxPosition.z);

            foreach (Ghost g in ghostList)
            {
                Vector3 auxMinP = new Vector3(g.transform.localPosition.x - offsetX / 2, minPosition.y, g.transform.localPosition.z - offsetY / 2);
                Vector3 auxMaxP = new Vector3(g.transform.localPosition.x + offsetX / 2, minPosition.y, g.transform.localPosition.z + offsetY / 2);

                if (auxPos.x >= auxMinP.x && auxPos.x <= auxMaxP.x)
                {
                    if (auxPos.z >= auxMinP.z && auxPos.z <= auxMaxP.z)
                    {
                        repeat = true;
                    }
                }
            }

        } while (repeat);

        return auxPos;
    }

    private void GhostDied(Ghost g)
    {
        ghostList.Remove(g);
    }
}
