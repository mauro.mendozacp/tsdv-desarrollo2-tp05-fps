﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        string path = GameManager.Instance.HighScorePath;
        FileStream fs;

        if (!File.Exists(path))
            fs = File.Create(path);
        else
            fs = File.Open(path, FileMode.Append);

        BinaryWriter bw = new BinaryWriter(fs);
        bw.Write(GameManager.Instance.Score);

        fs.Close();
        bw.Close();

        Cursor.lockState = CursorLockMode.Confined;
    }
}
