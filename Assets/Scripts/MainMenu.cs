﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject menu;
    [SerializeField] private GameObject highscore;

    public void StartGame()
    {
        SceneManager.LoadScene(GameManager.Instance.GameplayScene);
    }

    public void OpenHighScore()
    {
        menu.SetActive(false);
        highscore.SetActive(true);
    }

    public void OpenMenu()
    {
        menu.SetActive(true);
        highscore.SetActive(false);
    }
}
