﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public string HighScorePath { get; } = "highscore.dat";

    public int MainMenuScene { get; } = 0;
    
    public int GameplayScene { get; } = 1;

    public int GameOverScene { get; } = 2;

    public int Score { get; set; }
    public bool GameOver { get; set; }
    public bool WinGame { get; set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void ResetGame()
    {
        Score = 0;
        GameOver = false;
        WinGame = false;
    }
}
