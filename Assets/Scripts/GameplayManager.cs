﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.Score = 0;
        GameManager.Instance.GameOver = false;
        GameManager.Instance.WinGame = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.GameOver)
        {
            SceneManager.LoadScene(GameManager.Instance.GameOverScene);
        }
    }
}
