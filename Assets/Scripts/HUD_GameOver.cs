﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class HUD_GameOver : MonoBehaviour
{
    [SerializeField] private TMPro.TMP_Text scoreValue;

    void Update()
    {
        scoreValue.text = GameManager.Instance.Score.ToString();
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene(GameManager.Instance.MainMenuScene);
    }
}
