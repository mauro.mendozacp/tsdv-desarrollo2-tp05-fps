﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    private Player player;
    public int damage;
    public int score;

    [SerializeField] private float rangeToExplode;
    [SerializeField] private float explotionTemp;
    [SerializeField] private float distanceExplotion;

    private bool died;
    private bool explotionActivated;
    private float timer;

    public delegate void BombKilledAction(Bomb b);
    public static BombKilledAction OnBombKilledAsStatic;

    [SerializeField] private GameObject particle;
    [SerializeField] private GameObject bomb;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        explotionActivated = false;
        died = false;
        timer = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (!explotionActivated) {
             explotionActivated = CheckPlayerDistance(rangeToExplode);
         }
         else
         {
             timer += Time.deltaTime;
             if (timer >= explotionTemp)
             {
                DestroyBomb();
             }
         }
    }

    bool CheckPlayerDistance(float range)
    {
        if (Vector3.Distance(transform.position, player.transform.position) <= range)
            return true;

        return false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player.gameObject)
        {
            DestroyBomb();
        }
    }

    public void DestroyBomb()
    {
        if (!died)
        {
            died = true;

            if(!particle.activeSelf)
            {
                particle.SetActive(true);
                bomb.SetActive(false);
            }

            if (CheckPlayerDistance(distanceExplotion))
                player.ApplyDamage(damage);

            if (OnBombKilledAsStatic != null)
                OnBombKilledAsStatic(this);

            Destroy(gameObject, 2);
        }
    }
}
