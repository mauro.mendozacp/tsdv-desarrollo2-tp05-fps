﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] private string objectiveLayer;
    [SerializeField] private int damage;
    [SerializeField] private float speed;
    [SerializeField] private float duration;

    private float timer = 0f;

    public void InitForce(Vector3 dir)
    {
        GetComponent<Rigidbody>().AddForce(speed * dir, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= duration)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (LayerMask.LayerToName(other.gameObject.layer) == objectiveLayer)
        {
            other.gameObject.GetComponent<Ghost>().RecieveDamage(damage);
            Destroy(gameObject);
        }
    }
}
