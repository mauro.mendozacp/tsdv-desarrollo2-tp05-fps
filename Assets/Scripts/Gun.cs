﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    private Camera cam;

    [SerializeField] private float maxDistance;
    [SerializeField] private string objectiveLayer;

    [SerializeField] private GameObject weapon1;
    [SerializeField] private GameObject weapon2;

    [SerializeField] private GameObject ballList;
    [SerializeField] private GameObject ball;
    [SerializeField] private Transform startBall;
    

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        Shoot();
        ChangeWeapon();
        ThrowBall();
    }

    private void Shoot()
    {
        Vector3 mousePos = Input.mousePosition;
        Ray ray = cam.ScreenPointToRay(mousePos);

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, maxDistance))
            {
                string layerhitted = LayerMask.LayerToName(hit.transform.gameObject.layer);

                if (layerhitted == objectiveLayer)
                {
                    hit.transform.gameObject.GetComponent<Bomb>().DestroyBomb();
                    GameManager.Instance.Score += hit.transform.gameObject.GetComponent<Bomb>().score;
                }
            }
        }
    }

    private void ChangeWeapon()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (!weapon1.activeSelf)
            {
                weapon1.SetActive(true);
                weapon2.SetActive(false);
            }
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if (!weapon2.activeSelf)
            {
                weapon1.SetActive(false);
                weapon2.SetActive(true);
            }
        }
    }

    private void ThrowBall()
    {
        if (weapon2.activeSelf)
        {
            if (Input.GetMouseButtonDown(1))
            {
                GameObject b = Instantiate(ball, startBall.position, Quaternion.identity);
                b.GetComponent<Ball>().InitForce(transform.forward);
                b.transform.parent = ballList.transform;
            }
        }
    }
}
