﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxManager : MonoBehaviour
{
    [SerializeField] private float spawnDelay;
    [SerializeField] private GameObject boxPrefab;

    [SerializeField] private Vector3 minPosition;
    [SerializeField] private Vector3 maxPosition;
    [SerializeField] private float offsetX = 5;
    [SerializeField] private float offsetY = 5;

    private List<Box> boxList = new List<Box>();
    private float timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        Box.OnBoxKilledAsStatic += BoxDied;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > spawnDelay)
        {
            timer = 0;

            GameObject box = Instantiate(boxPrefab);
            Box b = box.GetComponent<Box>();

            box.name = "Box " + (boxList.Count + 1);
            box.transform.position = GetRandomPosition();
            box.transform.parent = transform;

            boxList.Add(b);
        }
    }

    public Vector3 GetRandomPosition()
    {
        Vector3 auxPos = Vector3.zero;
        bool repeat = false;

        do
        {
            repeat = false;

            auxPos.x = Random.Range(minPosition.x, maxPosition.x);
            auxPos.y = minPosition.y;
            auxPos.z = Random.Range(minPosition.z, maxPosition.z);

            foreach (Box b in boxList)
            {
                Vector3 auxMinP = new Vector3(b.transform.localPosition.x - offsetX / 2, minPosition.y, b.transform.localPosition.z - offsetY / 2);
                Vector3 auxMaxP = new Vector3(b.transform.localPosition.x + offsetX / 2, minPosition.y, b.transform.localPosition.z + offsetY / 2);

                if (auxPos.x >= auxMinP.x && auxPos.x <= auxMaxP.x)
                {
                    if (auxPos.z >= auxMinP.z && auxPos.z <= auxMaxP.z)
                    {
                        repeat = true;
                    }
                }
            }

        } while (repeat);

        return auxPos;
    }

    private void BoxDied(Box b)
    {
        boxList.Remove(b);
    }
}
