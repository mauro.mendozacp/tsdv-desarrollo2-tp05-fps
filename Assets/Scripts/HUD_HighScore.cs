﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD_HighScore : MonoBehaviour
{
    [SerializeField] private TMPro.TMP_Text[] hsTexts = new TMPro.TMP_Text[6];
    private List<int> hsList = new List<int>();

    // Start is called before the first frame update
    void Start()
    {
        string path = GameManager.Instance.HighScorePath;
        FileStream fs;

        if (File.Exists(path))
        {
            fs = File.OpenRead(path);
            BinaryReader br = new BinaryReader(fs);

            while (br.PeekChar() != -1)
            {
                hsList.Add(br.ReadInt32());
            }

            fs.Close();
            br.Close();

            hsList.Sort();
            hsList.Reverse();

            int index = 0;
            foreach (int hs in hsList)
            {
                hsTexts[index].text = hs.ToString();
                index++;

                if (index >= hsTexts.Length)
                    break;
            }
        }
    }
}
