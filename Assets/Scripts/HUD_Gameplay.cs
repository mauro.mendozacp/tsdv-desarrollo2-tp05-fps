﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD_Gameplay : MonoBehaviour
{
    [SerializeField] private TMPro.TMP_Text scoreValue;
    [SerializeField] private TMPro.TMP_Text healthValue;

    [SerializeField] private GameObject player;

    // Update is called once per frame
    void Update()
    {
        scoreValue.text = GameManager.Instance.Score.ToString();
        healthValue.text = player.GetComponent<Player>().health.ToString();
    }
}
