﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    enum SpawnType
    {
        Bomb,
        Box,
        Ghost
    }

    [SerializeField] private SpawnType spawnType;
    [SerializeField] private GameObject prefab;
    public List<GameObject> SpawnList { get; set; } = new List<GameObject>();

    [SerializeField] private Vector3 minPosition;
    [SerializeField] private Vector3 maxPosition;
    [SerializeField] private float offsetX;
    [SerializeField] private float offsetY;

    [SerializeField] private float spawnDelay;
    private float timer = 0;

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > spawnDelay)
        {
            timer = 0;

            GameObject goSpawn = Instantiate(prefab);
            goSpawn.transform.position = GetRandomBombPosition();

            switch (spawnType)
            {
                case SpawnType.Bomb:
                    goSpawn.name = "Bomb " + (SpawnList.Count + 1);
                    break;
                case SpawnType.Box:
                    goSpawn.name = "Box " + (SpawnList.Count + 1);
                    break;
                case SpawnType.Ghost:
                    break;
                default:
                    break;
            }

            goSpawn.transform.parent = transform;
            SpawnList.Add(goSpawn);
        }
    }

    public Vector3 GetRandomBombPosition()
    {
        Vector3 auxPos = Vector3.zero;
        bool repeat = false;

        do
        {
            repeat = false;

            auxPos.x = Random.Range(minPosition.x, maxPosition.x);
            auxPos.y = minPosition.y;
            auxPos.z = Random.Range(minPosition.z, maxPosition.z);

            foreach (GameObject s in SpawnList)
            {
                Vector3 auxMinP = new Vector3(s.transform.localPosition.x - offsetX / 2, minPosition.y, s.transform.localPosition.z - offsetY / 2);
                Vector3 auxMaxP = new Vector3(s.transform.localPosition.x + offsetX / 2, minPosition.y, s.transform.localPosition.z + offsetY / 2);

                if (auxPos.x >= auxMinP.x && auxPos.x <= auxMaxP.x)
                {
                    if (auxPos.z >= auxMinP.z && auxPos.z <= auxMaxP.z)
                    {
                        repeat = true;
                    }
                }
            }

        } while (repeat);

        return auxPos;
    }

    private void DestroyObject(GameObject go, int score)
    {
        SpawnList.Remove(go);
    }
}
