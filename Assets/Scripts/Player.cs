﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] public int health;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            GameManager.Instance.GameOver = true;
            GameManager.Instance.WinGame = false;
        }
    }

    public void ApplyDamage(int damage)
    {
        int auxHealth = health - damage;

        if (auxHealth < 0)
            health = 0;
        else
            health = auxHealth;
    }
}
