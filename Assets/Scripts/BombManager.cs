﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombManager : MonoBehaviour
{
    [SerializeField] private float spawnDelay;
    [SerializeField] private GameObject bombPrefab;

    [SerializeField] private Vector3 minPosition;
    [SerializeField] private Vector3 maxPosition;
    [SerializeField] private float offsetX;
    [SerializeField] private float offsetY;

    private List<Bomb> bombList = new List<Bomb>();
    private float timer = 0;

    private void Start()
    {
        Bomb.OnBombKilledAsStatic += BombDied;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > spawnDelay)
        {
            timer = 0;

            GameObject bomb = Instantiate(bombPrefab);
            
            Bomb b = bomb.GetComponent<Bomb>();

            bomb.name = "Bomb " + (bombList.Count + 1);
            bomb.transform.position = GetRandomPosition();
            bomb.transform.parent = transform;

            bombList.Add(b);
        }
    }

    public Vector3 GetRandomPosition()
    {
        Vector3 auxPos = Vector3.zero;
        bool repeat = false;

        do
        {
            repeat = false;

            auxPos.x = Random.Range(minPosition.x, maxPosition.x);
            auxPos.y = minPosition.y;
            auxPos.z = Random.Range(minPosition.z, maxPosition.z);

            foreach (Bomb b in bombList)
            {
                Vector3 auxMinP = new Vector3(b.transform.localPosition.x - offsetX / 2, minPosition.y, b.transform.localPosition.z - offsetY / 2);
                Vector3 auxMaxP = new Vector3(b.transform.localPosition.x + offsetX / 2, minPosition.y, b.transform.localPosition.z + offsetY / 2);

                if (auxPos.x >= auxMinP.x && auxPos.x <= auxMaxP.x)
                {
                    if (auxPos.z >= auxMinP.z && auxPos.z <= auxMaxP.z)
                    {
                        repeat = true;
                    }
                }
            }

        } while (repeat);

        return auxPos;
    }

    private void BombDied(Bomb b)
    {
        bombList.Remove(b);
    }
}
