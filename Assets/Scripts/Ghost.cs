﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : MonoBehaviour
{
    private Player player;
    [SerializeField] private int damage;
    [SerializeField] private int health;
    [SerializeField] private int score;
    [SerializeField] private float speed;

    [SerializeField] private float rangeToWalk;
    [SerializeField] private float idleDuration;
    [SerializeField] private float attackCooldown;
    [SerializeField] private float distanceToWait;
    [SerializeField] private float distanceToFollow;
    [SerializeField] private float distanceToAttack;

    private bool died;
    private bool wait;
    private float timer;

    private Vector3 target;

    public delegate void GhostKilledAction(Ghost g);
    public static GhostKilledAction OnGhostKilledAsStatic;

    enum GhostState
    {
        Idle,
        GoToPosition,
        Follow
    }
    private GhostState state;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        state = GhostState.Idle;
        died = false;
        timer = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateState();
        CheckHealth();
    }

    void UpdateState()
    {
        if (!died)
        {
            timer += Time.deltaTime;

            switch (state)
            {
                case GhostState.Idle:
                    if (timer >= idleDuration)
                    {
                        Vector3 pos = transform.position;

                        target = Vector3.zero;
                        target.x = Random.Range(pos.x - rangeToWalk, pos.x + rangeToWalk);
                        target.y = pos.y;
                        target.z = Random.Range(pos.z - rangeToWalk, pos.z + rangeToWalk);

                        state = GhostState.GoToPosition;
                        timer = 0;
                    }

                    break;
                case GhostState.GoToPosition:
                    Vector3 dir = target - transform.position;
                    transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

                    if (Vector3.Distance(transform.position, target) <= distanceToWait)
                    {
                        state = GhostState.Idle;
                        timer = 0;
                    }

                    break;
                case GhostState.Follow:
                    if (Vector3.Distance(transform.position, target) >= distanceToFollow)
                    {
                        state = GhostState.Idle;
                        timer = 0;
                    }

                    if (!wait)
                    {
                        target = player.transform.position;
                        target.y = 1;

                        Vector3 dir2 = target - transform.position;
                        transform.Translate(dir2.normalized * speed * Time.deltaTime, Space.World);
                        transform.LookAt(target);

                        if (Vector3.Distance(transform.position, target) <= distanceToAttack)
                        {
                            AttackPlayer();
                        }
                    }
                    else
                    {
                        if (timer >= attackCooldown)
                        {
                            wait = false;
                        }
                    }

                    break;
                default:
                    break;
            }

            if (CheckPlayerDistance(distanceToFollow) && state != GhostState.Follow)
            {
                state = GhostState.Follow;
            }
        }
    }

    void AttackPlayer()
    {
        player.ApplyDamage(damage);
        wait = true;
        timer = 0;
    }

    bool CheckPlayerDistance(float range)
    {
        if (Vector3.Distance(transform.position, player.transform.position) <= range)
            return true;

        return false;
    }

    void CheckHealth()
    {
        if (health <= 0)
        {
            DestroyGhost();
        }
    }

    void DestroyGhost()
    {
        if (!died)
        {
            died = true;

            GameManager.Instance.Score += score;

            if (OnGhostKilledAsStatic != null)
                OnGhostKilledAsStatic(this);

            Destroy(gameObject, 1);
        }
    }

    public void RecieveDamage(int damage)
    {
        int auxHealth = health - damage;

        if (auxHealth < 0)
            health = 0;
        else
            health = auxHealth;
    }
}
