﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    private Player player;
    public int score;

    [SerializeField] private GameObject particle;
    [SerializeField] private GameObject box;

    public delegate void BoxKilledAction(Box b);
    public static BoxKilledAction OnBoxKilledAsStatic;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player.gameObject)
        {
            GameManager.Instance.Score += score;
            DestroyBox();
        }
    }

    public void DestroyBox()
    {
        if (OnBoxKilledAsStatic != null)
            OnBoxKilledAsStatic(this);

        if (!particle.activeSelf)
        {
            particle.SetActive(true);
            box.SetActive(false);
        }
            
        Destroy(gameObject, 2);
    }
}
